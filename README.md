# Games List

This project was generated with React, webpack, emotion and styled-system among other technologies. This README document provides information about the development of an application cappable to manage a games list retrieved from an API.

## System requirements

- node 12.14.1
- npm 6.13.4

http://localhost:8080/

## Demo application website:

https://kingtest.web.app/

Please bear in mind that the server should be deployed in http://localhost:12345

## Business requirements:

- The end product should be a portfolio of my favorite King games.
- To add games to the fa‐ vorites I need to be able to search and browse through all the games on king.com.
- There should be a way of adding and removing them from a personal game portfolio.
- There should be a way of browse and search through the portfolio once one or more games have been added to it.
- a view for each game with the possibility to add/remove them from the portfolio, and the possibility to play (the actual playing is not required, just show that there is a way to navigate there, it doesn't have to do anything). This requirement was not implemented due to time restrictions.
- The application is usable both in desktop and mobile devices.
- A unit test suite was implemented inside the GamesManager component (reducer).
