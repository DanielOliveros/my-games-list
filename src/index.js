import React from 'react';
import ReactDOM from 'react-dom';
import { ThemeProvider } from '@emotion/react';
import theme from '@/theme';
import GamesManager from './components/GamesManager';

ReactDOM.render(
  <React.StrictMode>
    <ThemeProvider theme={theme}>
      <GamesManager />
    </ThemeProvider>
  </React.StrictMode>,
  document.getElementById('app'),
);
