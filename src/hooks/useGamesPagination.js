import { useState, useEffect } from 'react';

const INITIAL_PAGE = 0;
const GAMES_PER_PAGE = 6;
export function useGamesPagination({ games }) {
  const [page, setPage] = useState(INITIAL_PAGE);
  const [gamesToShow, setGamesToShow] = useState(games.slice(INITIAL_PAGE, GAMES_PER_PAGE));

  useEffect(() => {
    setGamesToShow(games.slice(INITIAL_PAGE, GAMES_PER_PAGE));
  }, [games]);

  useEffect(() => {
    if (page === INITIAL_PAGE) return;
    const offset = page * GAMES_PER_PAGE;
    const newPage = games.slice(offset, offset + GAMES_PER_PAGE);
    setGamesToShow((prev) => prev.concat(newPage));
  }, [page]);

  return { gamesToShow, setPage };
}
