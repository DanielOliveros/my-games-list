import { API_URL } from './setings';

export default function getGames() {
  const apiUrl = `${API_URL}/games.json`;
  return fetch(apiUrl).then((res) => res.json());
}
