export default {
  breakpoints: ['700px'],
  colors: {
    primaryDarkest: '#795b16',
    primaryDark: '#b38600',
    primary: '#ffc000',
    primaryLight: '#ffd34d',
    primaryLightest: '#ffe9b8',
    white: '#ffffff',
    secondaryLightest: '#bdbdbd',
    secondaryLight: '#6c6c6c',
    secondary: '#464646',
    secondaryDark: '#202020',
    secondaryDarkest: '#252525',
    black: '#000000',
  },
  space: [0, 4, 8, 16, 32, 64, 128, 256, 512],
};
