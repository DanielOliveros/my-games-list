import React from 'react';
import GameCard from 'components/GameCard';
import Box from 'components/Box';
import Button from 'components/Button';
import { useGamesPagination } from '../../hooks/useGamesPagination';

const GamesGrid = ({ games, toggleFav }) => {
  const { gamesToShow, setPage } = useGamesPagination({ games });

  return (
    <>
      <Box
        display="flex"
        flexWrap="wrap"
        justifyContent="center"
        bg={['white', 'primaryLightest']}
        borderRadius="4px"
      >
        { gamesToShow && gamesToShow.map((game) => (
          <Box
            margin="16px"
            key={game.name}
            alignSelf="center"
            maxWidth={['100%', '300px']}
          >
            <GameCard game={game} toggleFav={toggleFav} />
          </Box>
        ))}
      </Box>
      {
        gamesToShow.length < games.length && (
        <Box margin="20px auto">
          <Button onClick={() => setPage((prevPage) => prevPage + 1)}>Show more</Button>
        </Box>
        )
      }
    </>
  );
};
export default GamesGrid;
