import styled from '@emotion/styled';
import {
  space, layout, color, typography, border, flexbox,
} from 'styled-system';

const Box = styled('div')(
  {
    boxSizing: 'border-box',
  },
  space,
  layout,
  color,
  typography,
  border,
  flexbox,
);

export default Box;
