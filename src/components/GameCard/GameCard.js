import React, { useEffect, useState } from 'react';
import Box from 'components/Box';
import styled from '@emotion/styled';
import css from '@styled-system/css';
import FavIcon from 'components/icons/FavIcon';
import NotFavIcon from 'components/icons/NotFavIcon';

function gameImage(short) {
  return `http://www.royalgames.com/images/games/${short}/tournamentPage/${short}_764x260.jpg`;
}

const StyledCard = styled(Box)(
  css({
    display: 'flex',
    flexDirection: 'column',
    borderRadius: '4px',
    overflow: 'hidden',
    boxShadow: '0 2px 4px 0 rgb(0 0 0 / 30%)',
  }),
);

const StyledImage = styled('img')(css({
  height: 'auto',
  width: '100%',
}));

const FavoriteToggler = ({ game, toggleFav }) => {
  const [isFavState, setIsFavState] = useState(game.favorite);
  useEffect(() => {
    setIsFavState(game.favorite);
  }, [game.favorite]);
  const clickHandler = () => {
    toggleFav(game.name);
  };
  return (
    <Box onClick={clickHandler} height="28px" width="28px" css={{ cursor: 'pointer' }}>
      {isFavState ? (
        <FavIcon />
      ) : (
        <NotFavIcon />
      )}
    </Box>
  );
};

const GameCard = ({ game, toggleFav }) => (
  <StyledCard bg="white">
    <StyledImage src={gameImage(game.short)} alt="Game_Image" />
    <Box display="flex" flexGrow="1" justifyContent="space-between" alignItems="center" p={15}>
      <Box>{game.name}</Box>
      <FavoriteToggler game={game} toggleFav={toggleFav} />
    </Box>
  </StyledCard>
);

export default GameCard;
