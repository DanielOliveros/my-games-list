function gamesRepository() {
  const module = {};

  module.getGames = async function getGames() {
    const response = await fetch('http://localhost:12345/games.json');
    return response.json();
  };

  return module;
}

export default gamesRepository;
