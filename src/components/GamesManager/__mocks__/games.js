export default [
  {
    name: '8-Ball Pool',
    short: 'eightballpool',
    url: '/spel/sportspel/8-ball-pool/?language=sv',
    tags: '',
    hasBoosters: false,
  },
  {
    name: '9-Ball Pool',
    short: 'nineballpool',
    url: '/spel/sportspel/9-ball-pool/?language=sv',
    tags: '',
    hasBoosters: false,
  },
  {
    name: 'Abracadabra',
    short: 'abracadabra',
    url: '/spel/strategispel/abracadabra/?language=sv',
    tags: '',
    hasBoosters: false,
  },
];
