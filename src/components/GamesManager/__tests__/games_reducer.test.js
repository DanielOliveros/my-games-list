import reducer, { getInitialState } from '../reducer';
import * as actions from '../actions';
import gamesMock from '../__mocks__/games';

const ERROR_MESSAGE = 'Error when loading results';
const GAME_NAME = '9-Ball Pool';

function getExpectedState({
  loading = false, error = '', data = undefined, filteredData = undefined,
}) {
  return {
    loading,
    error,
    data,
    filteredData,
  };
}

function describeCaseForAction({
  message,
  mutationObject,
  action,
  payload,
  initialState = getInitialState(),
}) {
  describe(message, () => {
    it('should return a new reference to state', () => {
      const newState = reducer(initialState, action(payload));
      expect(initialState).not.toBe(newState);
    });
    it('should set PROPETY on state', () => {
      const newState = reducer(initialState, action(payload));
      expect(newState).toEqual(getExpectedState(mutationObject));
    });
  });
}

describe('Games reducer tests', () => {
  describeCaseForAction({
    message: 'LOAD_DATA reducer test',
    mutationObject: { loading: true },
    action: actions.loadData,
  });
  describeCaseForAction({
    message: 'LOAD_DATA_ERROR reducer test',
    mutationObject: { loading: false, error: ERROR_MESSAGE },
    action: actions.loadDataError,
    payload: ERROR_MESSAGE,
  });
  describeCaseForAction({
    message: 'LOAD_DATA_SUCCESS reducer test',
    mutationObject: { loading: false, data: gamesMock, filteredData: gamesMock },
    action: actions.loadDataSuccess,
    payload: gamesMock,
  });
  describeCaseForAction({
    message: 'FILTER_DATA reducer test',
    mutationObject: { filteredData: [gamesMock[0]] },
    action: actions.filterData,
    payload: [gamesMock[0]],
  });
  describeCaseForAction({
    message: 'TOGGLE_LIKE reducer test',
    mutationObject: { data: gamesMock, filteredData: gamesMock },
    action: actions.toggleLike,
    payload: GAME_NAME,
    initialState: {
      ...getInitialState(),
      data: gamesMock,
      filteredData: gamesMock,
    },
  });
});
