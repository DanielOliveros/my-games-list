import {
  LOAD_DATA, LOAD_DATA_ERROR, LOAD_DATA_SUCCESS, FILTER_DATA, TOGGLE_LIKE,
} from './actions/action_types';

export function getInitialState() {
  return {
    loading: false,
    error: '',
    data: undefined,
    filteredData: undefined,
  };
}

function toggleLike(state, gameName) {
  const game = state.data.find((item) => item.name === gameName);
  game.favorite = !game.favorite;
}

export default function reducer(state = getInitialState(), action) {
  switch (action.type) {
    case LOAD_DATA:
      return { ...state, loading: true };
    case LOAD_DATA_ERROR:
      return { ...state, loading: false, error: action.payload };
    case LOAD_DATA_SUCCESS:
      return {
        loading: false, error: '', data: action.payload, filteredData: action.payload,
      };
    case FILTER_DATA:
      return { ...state, filteredData: action.payload };
    case TOGGLE_LIKE:
      toggleLike(state, action.payload);
      return { ...state };
    default:
      return state;
  }
}
