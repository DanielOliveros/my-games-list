export const LOAD_DATA = 'LOAD_DATA';
export const LOAD_DATA_SUCCESS = 'LOAD_DATA_SUCCESS';
export const LOAD_DATA_ERROR = 'LOAD_DATA_ERROR';
export const FILTER_DATA = 'FILTER_DATA';
export const TOGGLE_LIKE = 'TOGGLE_LIKE';
