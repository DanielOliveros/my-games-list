import {
  LOAD_DATA, LOAD_DATA_ERROR, LOAD_DATA_SUCCESS, FILTER_DATA, TOGGLE_LIKE,
} from './action_types';

function createAction(type, payload) {
  return { type, payload };
}

export function loadData() {
  return createAction(LOAD_DATA);
}

export function loadDataError(payload) {
  return createAction(LOAD_DATA_ERROR, payload);
}

export function loadDataSuccess(payload) {
  return createAction(LOAD_DATA_SUCCESS, payload);
}

export function filterData(payload) {
  return createAction(FILTER_DATA, payload);
}

export function toggleLike(payload) {
  return createAction(TOGGLE_LIKE, payload);
}
