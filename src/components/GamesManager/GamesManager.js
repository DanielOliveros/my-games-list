import React, { useEffect, useReducer } from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from 'react-router-dom';
import Header from 'components/Header';
import Box from 'components/Box';
import AllGames from 'components/AllGames';
import FavoriteGames from 'components/FavoriteGames';
import debounce from 'utils/debounce';
import getGames from 'services/getGames';
import reducer, { getInitialState } from './reducer';
import * as actions from './actions';

const GamesManager = () => {
  const [state, dispatch] = useReducer(reducer, getInitialState());

  useEffect(() => {
    dispatch(actions.loadData());
    getGames()
      .then((data) => {
        dispatch(actions.loadDataSuccess(data.games));
      })
      .catch(() => {
        dispatch(actions.loadDataError('Error getting games list'));
      });
  }, []);

  const searchGames = debounce((event) => {
    const searchWord = event.target.value;
    const filteredData = state.data.filter(
      ({ name }) => name.toLowerCase().indexOf(searchWord.toLowerCase()) !== -1,
    );
    dispatch(actions.filterData(filteredData));
  }, 250);
  return (
    <>
      <Box width="100%" pb="20px">
        <Header onSearch={searchGames} />
      </Box>
      {state.filteredData && (
        <Router>
          <Switch>
            <Route path="/favorite-games">
              <FavoriteGames
                games={state.filteredData.filter((item) => item.favorite)}
                toggleFav={(gameName) => dispatch(actions.toggleLike(gameName))}
              />
            </Route>
            <Route path="/">
              <AllGames
                games={state.filteredData}
                toggleFav={(gameName) => dispatch(actions.toggleLike(gameName))}
              />
            </Route>
          </Switch>
        </Router>
      )}
    </>
  );
};

export default GamesManager;
