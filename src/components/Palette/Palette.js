import React from 'react';
import Box from 'components/Box';
import theme from '@/theme';

const Palette = () => (
  <Box p={1} width={200}>
    {Object.values(theme.colors).map((color) => (
      <Box bg={color} key={color} height={32} />
    ))}
  </Box>
);

export default Palette;
