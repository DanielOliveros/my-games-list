import React from 'react';
import Box from 'components/Box';
import styled from '@emotion/styled';
import css from '@styled-system/css';

const StyledInput = styled('input')(
  css({
    border: 'none',
    borderRadius: '15px',
    height: '24px',
    padding: '4px 12px',
    outline: 'none',
  }),
);

const Header = ({ onSearch }) => (
  <Box
    width={1}
    height={64}
    bg="primary"
    display="flex"
    justifyContent="center"
    alignItems="center"
    css={{ position: 'fixed', boxShadow: '0 4px 12px 0 rgb(101 103 106 / 10%);' }}
  >
    <StyledInput type="text" id="search" name="search" placeholder="Search:" onChange={onSearch} />
  </Box>
);

export default Header;
