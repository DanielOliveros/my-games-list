import React from 'react';
import styled from '@emotion/styled';
import css from '@styled-system/css';
import shouldForwardProp from '@styled-system/should-forward-prop';

const StyledButton = styled('button', { shouldForwardProp })(css({
  alignItems: 'center',
  appearance: 'none',
  paddingX: '32px',
  paddingY: '12px',
  borderRadius: '22px',
  outline: 0,
  cursor: 'pointer',
  backgroundColor: 'primary',
  border: 'none',
  color: 'white',
  '&:hover': {
    backgroundColor: 'primaryDark',
  },
}));
const Button = ({ children, ...rest }) => (<StyledButton {...rest}>{children}</StyledButton>);

export default Button;
