import React from 'react';
import GamesGrid from 'components/GamesGrid';
import Box from 'components/Box';
import Button from 'components/Button';
import { useHistory } from 'react-router-dom';

const AllGames = ({ games, toggleFav }) => {
  const history = useHistory();
  return (
    <Box display="flex" justifyContent="center">
      <Box
        maxWidth="1100px"
        margin={['60px 0',
          '60px 20px']}
        display="flex"
        flexDirection="column"
        justifyContent="center"
      >
        <Box width="100%" display="flex" justifyContent="center">
          <Button onClick={() => { history.push('/favorite-games'); }}>See favorites</Button>
        </Box>
        <Box alignSelf="center" color="secondaryLight"><h2>All games:</h2></Box>
        {games && (
        <GamesGrid
          games={games}
          toggleFav={toggleFav}
        />
        )}
      </Box>
    </Box>
  );
};

export default AllGames;
