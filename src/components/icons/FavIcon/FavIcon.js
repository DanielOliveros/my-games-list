import React from 'react';

const FavIcon = () => (
  <svg xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" width="30" height="30" viewBox="0 0 30 30">
    <defs>
      <radialGradient id="a" cx="0.5" cy="0.5" r="0.5" gradientUnits="objectBoundingBox">
        <stop offset="0" stopColor="#ffe19a" />
        <stop offset="1" stopColor="#ffc234" />
      </radialGradient>
    </defs>
    <path d="M16,2l4,10H30l-8,7,3,11-9-7L7,30l3-11L2,12H12Z" transform="translate(-1 -1)" stroke="#fff" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" fill="url(#a)" />
  </svg>
);

export default FavIcon;
